# Windows version of Ecdosis tutorials
To install Ecdosis tutorials for Windows download the ecdosis.win.zip package to your hard disk.

1. Unzip it in the usual way (by right-clicking on it and selecting 'Extract-all"). There is one folder inside the zip file called tutorials-win. When prompted where to store it, delete "tutorials-win" from the suggested file-path. Also you might like to adjust the location to your home folder or to Documents, NOT to Program Files. 
2. Launch Powershell or the DOS Command console. cd to the tutorials-win directory you created in step 1. If you didn't delete "tutorials-win" from the path in step 1 there will be a 
tutorials-win directory inside tutorials-win. In that case, please cd into the innermost tutorials-win directory.
3. Type .\create_links.bat
4. After the create links batch file has run you should be able to launch Ecdosis by typing .\ecdosis-run.bat
5. If Windows Defender complains that a program is trying to access the network and has been blocked please unblock it and 
enable it. This is because Ecdosis uses port 8081. You can permanently allow that port in Windows Defender if you like by running Windows Defender as administrator and opening up incoming port 8081. There are instructions at http://pureinfotech.com/open-port-firewall-windows-10/
6. Go to a browser and type http://localhost:8081/ into the url bar. This should take you to Ecdosis
7. To stop Ecdosis go back to the tutorials-win folder and type .\ecdosis-stop.bat
